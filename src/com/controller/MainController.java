package com.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
/*import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;*/
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.dao.PersDao;
import com.dao.SehirlerDao;
import com.model.Personel;
import com.model.Sehirler;

import net.sf.jasperreports.engine.JRException;


import net.sf.jasperreports.engine.JRException;

@Controller
public class MainController {

	@Autowired
	private PersDao personel_dao;
	
	@Autowired
	private SehirlerDao sehirler_dao;
 

	
	@RequestMapping(value={"/","/index"}, method=RequestMethod.GET)
	public ModelAndView defaultPage(){
		ModelAndView my_model=new ModelAndView();
		my_model.addObject("title","Hello Page");
		my_model.addObject("butunPersoneller", personel_dao.butunPersoneller());
		my_model.addObject("butunSehirler", sehirler_dao.butunSehirler());
		my_model.setViewName("index");		
		return my_model;
	}

	
	
	@RequestMapping(value="/silme/{personelid}",method=RequestMethod.GET)
	public ModelAndView silme(@PathVariable long personelid){
	personel_dao.sildao(personelid);
	   return new ModelAndView("redirect:/index");						
	}
	
	
	@RequestMapping(value = "/ekleme", method = RequestMethod.POST)
	public String saveEmployee(Personel personel) {
		personel_dao.ekleme(personel);
		return ("redirect:/index");
	}
	
	
	/*@RequestMapping(value="/index/guncelleme/{personelid}" , method=RequestMethod.GET)
	  public ModelAndView guncelle(@PathVariable long personelid){
	
		ModelAndView my_model=new ModelAndView();
		Personel personel=personel_dao.getPersonel(personelid);
		my_model.addObject("personel", personel );
		my_model.setViewName("guncelleme");
		return my_model;
	}*/
	
	
  @RequestMapping(value="/guncel", method = RequestMethod.POST)
    public String guncelleme(Personel personel){
	   personel_dao.guncelle(personel);
	   return ("redirect:/index");
   }
	
  
	@RequestMapping(value="/index2", method=RequestMethod.GET)
	public ModelAndView listeleme(){
		 ModelAndView my_model=new ModelAndView();
		 my_model.addObject("butunSehirler", sehirler_dao.butunSehirler());
		// my_model.addObject("title", "butunPersoneller");
		 my_model.setViewName("index2");
		 return my_model;
	}
	
	@RequestMapping(value="/silme2/{plakakod}",method=RequestMethod.GET)
	public ModelAndView silme2(@PathVariable long plakakod){
	sehirler_dao.silme2(plakakod);
	   return new ModelAndView("redirect:/index2");						
	}

	@RequestMapping(value = "/ekleme2", method = RequestMethod.POST)
	public String ekleme2(Sehirler sehir) {
		sehirler_dao.ekleme2(sehir);
		return ("redirect:/index2");
	}
	
	  @RequestMapping(value="/guncel2", method = RequestMethod.POST)
	    public String guncelleme2(Sehirler sehir){
		   sehirler_dao.guncelleme2(sehir);
		   return ("redirect:/index2");
	   }
	  
	/* 
	 @RequestMapping(value="/filtreli/{plakakod}", method=RequestMethod.POST)
	public ModelAndView fitreli(@PathVariable long plakakod){
		 ModelAndView my_model=new ModelAndView();
	//	personel_dao.filtreliListe(plakakod);		
	//	my_model.addObject("filtreliListe", personel_dao.filtreliListe(plakakod));
	//	listeleme_f(plakakod);
		return new ModelAndView("filtreli");
	
	}
	  
	@RequestMapping(value="/filtreli", method=RequestMethod.GET)
	public ModelAndView listeleme_f(long plakakod){
		 ModelAndView my_model=new ModelAndView();
		 my_model.addObject("filtreliListe",personel_dao.filtreliListe(plakakod));
		 my_model.setViewName("filtreli");
		 return my_model;
	}
	 */	  
	  
@RequestMapping(value="/filtreli", method=RequestMethod.POST)
public ModelAndView filtereli(@ModelAttribute(value="sehirler.plakakod") long plakakod){
	ModelAndView model = new ModelAndView();
	List<Personel> prs = null;
	
	   if(plakakod==0){
		   prs=personel_dao.butunPersoneller();
	   }
	   else{
		   prs=personel_dao.filtreliListe(plakakod);
	   }
	   model.addObject("butunPersoneller",prs);
	   model.setViewName("index");
	return model;
}


@RequestMapping(value = "/personelBilgi", method = RequestMethod.GET)
@ResponseBody
public void personelBilgi(HttpServletResponse response) throws JRException, IOException, SQLException {
personel_dao.personelBilgi(response);
}
	
@RequestMapping(value = {"filtreliRapor"/*,"/filtreliCalisanRapor/**"*/}, method = RequestMethod.GET)
public void filtreliRapor(HttpServletResponse response, @ModelAttribute(value="sehirler.plakakod") long plakakod) throws JRException, IOException, SQLException {
	personel_dao.filtreliRapor(response, Long.valueOf(plakakod));
}

	
}












