package com.dao;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.SessionFactory;
import org.hibernate.engine.jdbc.connections.spi.ConnectionProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.model.Personel;
import com.model.Sehirler;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;



@EnableTransactionManagement
@Transactional
@Repository("PersDao")
public class DaoImpl implements PersDao{
	@Autowired
	private SessionFactory mySession;
	List<Personel> liste;	
	List<Personel> my_list;
	@Override
	public List<Personel> butunPersoneller() {
		List<Personel> liste=(List<Personel>) mySession.getCurrentSession().createQuery("from Personel").list();
		
		if(liste.size()>0){
				return liste;
				}
		else
			return  null ;
	}

	@Override
	public Void sildao(long personelid ){
		String sorgu="delete from Personel where personelid= :personelid";	
		Query sorgu1=mySession.getCurrentSession().createSQLQuery(sorgu).setParameter("personelid", personelid);
				sorgu1.executeUpdate();
				return null;
	}

	@Override
	public void ekleme(Personel personel) {
	Sehirler x = new Sehirler();
	String sorgu="insert into personel (personelID, ad, soyad,dogum_tarihi,sehir) VALUES (personelid_seq.NEXTVAL, :ad , :soyad, :dogum_tarihi , :sehir)";	
	Query sorgu2=mySession.getCurrentSession().createSQLQuery(sorgu);
	sorgu2.setParameter("ad", personel.getAd());
	sorgu2.setParameter("soyad", personel.getSoyad());	
	//System.out.println(personel.getDogumTarihi().toString()+"***********************");
	sorgu2.setParameter("dogum_tarihi",personel.getDogumTarihi()/* == null ? null : personel.getDogumTarihi() */);
	sorgu2.setParameter("sehir",personel.sehirler.getPlakakod()/*41*/);
//	System.out.println(personel.sehırler.getSehir()+"*******");

	sorgu2.executeUpdate();
	}

	
	@Override
	public void guncelle(Personel personel) {
		
	//String sorgu="UPDATE personel SET ad = :ad , soyad= :soyad , dogumTarihi= :dogumTarihi where personelid= :personelid ";	
	String sorgu="UPDATE personel SET ad = :ad , soyad= :soyad , dogum_tarihi= :dogum_tarihi , sehir = :sehir where personelID = :personelID ";	
	Query sorgu3=mySession.getCurrentSession().createSQLQuery(sorgu);	
	getPersonel(personel.getPersonelid());
	sorgu3.setParameter("personelID", personel.getPersonelid());
	sorgu3.setParameter("ad", personel.getAd() );
	sorgu3.setParameter("soyad", personel.getSoyad());
	sorgu3.setParameter("dogum_tarihi",personel.getDogumTarihi() /* "1.1.2000"*/);
    sorgu3.setParameter("sehir",personel.sehirler.getPlakakod());
   
	sorgu3.executeUpdate();

	}

	@SuppressWarnings("unchecked")
	@Override
	public Personel getPersonel(long personelid) {
		
	
		List<Personel> my_list = new ArrayList<Personel>();
		my_list = (List<Personel>) mySession.getCurrentSession().createQuery("FROM Personel WHERE personelid= :personelid").setParameter("personelid", personelid).list();
		

		/*
		prs.setPersonelid(my_list.get(0).getPersonelid());
		prs.setAd(my_list.get(0).getAd());
		prs.setSoyad(my_list.get(0).getSoyad());
		prs.setDogumTarihi(my_list.get(0).getDogumTarihi());
		prs.sehirler.setPlakakod(my_list.get(0).sehirler.getPlakakod());
	
		*/
	//	return prs;
		
	return new Personel(my_list.get(0).getPersonelid(), my_list.get(0).getAd(), my_list.get(0).getSoyad(), my_list.get(0).getDogumTarihi());

	
	
	
	}
/*
	@RequestMapping(value = "/report3", method = RequestMethod.GET)
	@ResponseBody
	@Override
	public String raporIndir(HttpServletResponse response) throws JRException, IOException, SQLException {
	InputStream jasperStream = this.getClass().getResourceAsStream("/com/reports/report3.jasper");
	     Map<String,Object> parameters = new HashMap<String, Object>();
	     
	     Connection c = mySession.getSessionFactoryOptions().getServiceRegistry().getService(ConnectionProvider.class).getConnection();
	  
	  
	     JasperReport jasperReport = (JasperReport) JRLoader.loadObject(jasperStream);
	     jasperReport.setProperty("net.sf.jasperreports.query.executer.factory.plsql"
	                 ,"com.jaspersoft.jrx.query.PlSqlQueryExecuterFactory");
	     
	     parameters.put("a","aa");
	     
	     try {
	     JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, c);
	     response.setContentType("application/x-pdf");
	     response.setHeader("Content-disposition", "inline; filename=report3.pdf");
	     final OutputStream outStream = response.getOutputStream();
	     JasperExportManager.exportReportToPdfStream(jasperPrint, outStream);
	     }catch (Exception e) {

	    System.out.println("Hata : "+e.getMessage());
	} 
	     return null;
	} 

*/


	@Override
	public List<Personel> filtreliListe(long sehir) {
List<Personel> liste=(List<Personel>) mySession.getCurrentSession().createQuery("from Personel where sehir = :sehir").setParameter("sehir",sehir).list();
		return liste;
	}


//	@RequestMapping(value = "/personelBilgi", method = RequestMethod.GET)
//	@ResponseBody
	@Override
	public String personelBilgi(HttpServletResponse response) throws JRException, IOException, SQLException {
		InputStream jasperStream = this.getClass().getResourceAsStream("/com/reports/personelBilgi.jasper");
	     Map<String,Object> parameters = new HashMap<String, Object>();
	     
	     Connection c = mySession.getSessionFactoryOptions().getServiceRegistry().getService(ConnectionProvider.class).getConnection();
	  
	  
	     JasperReport jasperReport = (JasperReport) JRLoader.loadObject(jasperStream);
	     jasperReport.setProperty("net.sf.jasperreports.query.executer.factory.plsql"
	                 ,"com.jaspersoft.jrx.query.PlSqlQueryExecuterFactory");
	     
	     parameters.put("a","aa");
	     
	     try {
	     JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, c);
	     response.setContentType("application/x-pdf");
	     response.setHeader("Content-disposition", "inline; filename=personelBilgi.pdf");
	     final OutputStream outStream = response.getOutputStream();
	     JasperExportManager.exportReportToPdfStream(jasperPrint, outStream);
	     }catch (Exception e) {

	    System.out.println("Hata : "+e.getMessage());
	} 
	     return null;
	}

@Override
public String filtreliRapor(HttpServletResponse response, Long plakakod)
		throws JRException, IOException, SQLException {
	InputStream jasperStream = this.getClass().getResourceAsStream("/com/reports/filtreliRapor.jasper");

    HashMap map = new HashMap();

    Connection c = mySession.getSessionFactoryOptions().getServiceRegistry().getService(ConnectionProvider.class).getConnection();
    
    JasperReport jasperReport = (JasperReport) JRLoader.loadObject(jasperStream);

    jasperReport.setProperty("net.sf.jasperreports.query.executer.factory.plsql"

              ,"com.jaspersoft.jrx.query.PlSqlQueryExecuterFactory");

    map.put("x",plakakod);

    

    try {

    JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, map, c);

    response.setContentType("application/x-pdf");

    response.setHeader("Content-disposition", "inline; filename=filtreliRapor.pdf");

    final OutputStream outStream = response.getOutputStream();

    JasperExportManager.exportReportToPdfStream(jasperPrint, outStream);

    }catch (Exception e) {
   	 
   System.out.println("Hata : "+e.getMessage());

    } 

    return null;
}

	



}
