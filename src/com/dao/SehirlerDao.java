package com.dao;

import java.util.List;

import com.model.Personel;
import com.model.Sehirler;

public interface SehirlerDao {
	public List<Sehirler> butunSehirler() ;
	public Void silme2(long plakaKod);
	public void ekleme2(Sehirler sehir);
	public void guncelleme2(Sehirler sehir);
	
}
