package com.dao;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import com.model.Personel;

import net.sf.jasperreports.engine.JRException;



import net.sf.jasperreports.engine.JRException;

public interface PersDao {
	public List<Personel> butunPersoneller() ;
    public Void sildao(long personelid);
    public void ekleme(Personel personel);
    public void guncelle(Personel personel);
    public Personel getPersonel(long personelid);
    public List<Personel> filtreliListe(long sehir);
    String personelBilgi(HttpServletResponse response) throws JRException, IOException, SQLException;
    String filtreliRapor(HttpServletResponse response, Long plakakod) throws JRException, IOException, SQLException;
}
