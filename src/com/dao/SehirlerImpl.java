package com.dao;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import com.model.Personel;
import com.model.Sehirler;
@EnableTransactionManagement
@Transactional
@Repository("SehirlerDao")
public class SehirlerImpl implements SehirlerDao{
	@Autowired
	private SessionFactory mySession;
	@Override
	public List<Sehirler> butunSehirler() {
				
	List<Sehirler> sehirList=(List<Sehirler>)mySession.getCurrentSession().createQuery("from Sehirler").list();	
		return sehirList;
	}
	
	
	@Override
	public Void silme2(long plakaKod) {
		
	String sorgu="delete from sehirler where plakaKod= :plakaKod";
	Query  sorgu2=mySession.getCurrentSession().createSQLQuery(sorgu).setParameter("plakaKod", plakaKod);
		
	   sorgu2.executeUpdate();
	return null;
	}


	@Override
	public void ekleme2(Sehirler sehir) {
	String sorgu="insert into sehirler(plakaKod , sehir) values( :plakaKod ,:sehir)";
	Query sorgu2=mySession.getCurrentSession().createSQLQuery(sorgu);
	sorgu2.setParameter("plakaKod", sehir.getPlakakod());
	sorgu2.setParameter("sehir", sehir.getSehir());
	
	sorgu2.executeUpdate();
		
	}


	@Override
	public void guncelleme2(Sehirler sehir) {
		String sorgu="update sehirler set sehir= :sehir where plakaKod= :plakaKod";
		Query sorgu2=mySession.getCurrentSession().createSQLQuery(sorgu);
		sorgu2.setParameter("plakaKod", sehir.getPlakakod() );
		sorgu2.setParameter("sehir", sehir.getSehir());
		
		sorgu2.executeUpdate();
	}

}
