package com.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


@Entity
@NamedQuery(name="Sehirler.findAll", query="SELECT s FROM Sehirler s")
public class Sehirler implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long plakakod;

	private String sehir;

	//bi-directional many-to-one association to Personel
	@OneToMany(mappedBy="sehirler")
	private List<Personel> personels;

	public Sehirler() {
	}

	public long getPlakakod() {
		return this.plakakod;
	}

	public void setPlakakod(long plakakod) {
		this.plakakod = plakakod;
	}

	public String getSehir() {
		return this.sehir;
	}

	public void setSehir(String sehir) {
		this.sehir = sehir;
	}

	public List<Personel> getPersonels() {
		return this.personels;
	}

	public void setPersonels(List<Personel> personels) {
		this.personels = personels;
	}

	public Personel addPersonel(Personel personel) {
		getPersonels().add(personel);
		personel.setSehirler(this);

		return personel;
	}

	public Personel removePersonel(Personel personel) {
		getPersonels().remove(personel);
		personel.setSehirler(null);

		return personel;
	}

}