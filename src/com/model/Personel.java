package com.model;

import java.io.Serializable;
import javax.persistence.*;

@Entity

@NamedQuery(name="Personel.findAll", query="SELECT p FROM Personel p")
public class Personel implements Serializable {
	private static final long serialVersionUID = 1L;

    @Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long personelid;

	private String ad;

	@Column(name="DOGUM_TARIHI")
	private String dogumTarihi;

	private String soyad;

	//bi-directional many-to-one association to Sehirler
	@ManyToOne
	@JoinColumn(name="SEHIR")
	public Sehirler sehirler;

	public Personel(){
		
	}
	
	public Personel(long personelid, String ad , String soyad , String dogumTarihi ) {
		this.personelid=personelid;
		this.ad=ad;
		this.soyad=soyad;
		this.dogumTarihi=dogumTarihi;
	}

	public long getPersonelid() {
		return this.personelid;
	}

	public void setPersonelid(long personelid) {
		this.personelid = personelid;
	}

	public String getAd() {
		return this.ad;
	}

	public void setAd(String ad) {
		this.ad = ad;
	}

	public String getDogumTarihi() {
		return this.dogumTarihi;
	}

	public void setDogumTarihi(String dogumTarihi) {
		this.dogumTarihi = dogumTarihi;
	}

	public String getSoyad() {
		return this.soyad;
	}

	public void setSoyad(String soyad) {
		this.soyad = soyad;
	}

	public Sehirler getSehirler() {
		return this.sehirler;
	}

	public void setSehirler(Sehirler sehirler) {
		this.sehirler = sehirler;
	}
	
	
	
	
	

}