<%@ page language="java" contentType="text/html; utf-8"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>



<table border="1" BGCOLOR="#474787" width="50%">
   <tr>
       <th>PLAKA KODU</th><th>SEHIR</th>    
         
   </tr>
  
    <c:forEach items="${butunSehirler}" var="i">
     
  <tr>	
  
  <td bgcolor="#f7f1e3"><c:out  value="${i.plakakod}" /></td>
      <td bgcolor="#f7f1e3"><c:out value="${i.sehir}" /></td>
      <td bgcolor="#f7f1e3"><a href="silme2/${i.plakakod}">SIL</a></td>
  
	
  </tr>   
     </c:forEach>  

</table>


&nbsp; &nbsp;

<fieldset>
<legend>KAYIT EKLEME</legend>
<form:form method="POST" modelAttribute="sehir" action="ekleme2">
	<table>
		<tr>
			<td>Plaka Kod :</td>
			<td><input name="plakakod" type="text" /></td>
		</tr>
		<tr>
			<td>Sehir :</td>
			<td><input name="sehir" type="text" /></td>
		</tr>	
		<tr>
			<td></td>
			<td><input type="submit" value="EKLE" /></td>
		</tr>
	</table>
</form:form>


</fieldset>


&nbsp; &nbsp;


   <fieldset>
   <legend>GUNCELLEME</legend>
   <form:form method="post" modelAttribute="sehir" action="guncel2">
   <table>		
			<tr>
				<td>Plaka Kod:</td>
					<td><select name="plakakod">
						<c:forEach items="${butunSehirler}" var="item">
							<option value="<c:out value="${item.plakakod}" />"><c:out
									value="${item.plakakod}" /></option>
						</c:forEach>
					</select></td>	
			<tr>
				<td>Guncel Ad:</td>
				<td><input name="sehir" type="text" /></td>
			</tr>
				
			<tr>
				<td></td>
				<td><input type="submit" value="Guncelle" /></td>
			</tr>
		</table>
   </form:form>  
   </fieldset>
   
<a href="index">Onceki Sayfa</a> 

</body>
</html>