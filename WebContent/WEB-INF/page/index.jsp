<%@ page language="java" contentType="text/html; charset=ISO-8859-9"
    pageEncoding="ISO-8859-9"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>--Insert title here--</title>
</head>

<body>

<table border="1" BGCOLOR="#474787" width="50%">
   <tr>
       <th>Id</th><th>Ad</th><th>Soyad</th><th>Dogum Tarihi</th><th>Sehir</th>
   </tr>
  
    <c:forEach items="${butunPersoneller}" var="i">
     <TR>
     
       <td bgcolor="#f7f1e3"><c:out  value="${i.personelid}" /></td>
      <td bgcolor="#f7f1e3"><c:out value="${i.ad}" /></td>
        <td bgcolor="#f7f1e3"><c:out  value="${i.soyad}" /></td>
      <td bgcolor="#f7f1e3"><c:out value="${i.dogumTarihi}" /></td>
        <td bgcolor="#f7f1e3"><c:out  value="${i.sehirler.sehir}" /></td>
     	<td bgcolor="#f7f1e3" ><a href="silme/${i.personelid}">S�L</a></td>
    
    </TR>   
     </c:forEach>  
</table>

 &nbsp; &nbsp;

<fieldset>
<legend>KAYIT EKLEME</legend>
<form:form method="POST" modelAttribute="personel" action="ekleme">
	<table>
		<tr>
			<td>Ad :</td>
			<td><input name="ad" type="text" /></td>
		</tr>
		<tr>
			<td>Soyad :</td>
			<td><input name="soyad" type="text" /></td>
		</tr>
		
		<tr>
			<td>Do�um Tarihi :</td>
			<td><input name="dogumTarihi" type="text" /></td>
		</tr>
		
		<tr>
			<td>Sehir:</td>
					<td><select name="sehirler.plakakod">
						<c:forEach items="${butunSehirler}" var="item">
							<option value="<c:out value="${item.plakakod}" />"><c:out
									value="${item.sehir}" /></option>
						</c:forEach>
					</select></td>	
		</tr>
		
		<tr>
			<td></td>
			<td><input type="submit" value="EKLE" /></td>
		</tr>
	</table>
</form:form>
</fieldset>

&nbsp; &nbsp;


   <fieldset>
   <legend>G�NCELLEME</legend>
   <form:form method="post" modelAttribute="personel" action="guncel">
   <table>		
			<tr>
				<td>Personel :</td>
					<td><select name="personelid">
						<c:forEach items="${butunPersoneller}" var="item">
							<option value="<c:out value="${item.personelid}" />"><c:out
									value="${item.ad}" /></option>
						</c:forEach>
					</select></td>	
			<tr>
				<td>G�ncel Ad:</td>
				<td><input name="ad" type="text" /></td>
			</tr>
			
			<tr>
				<td>G�ncel Soyad:</td>
				<td><input name="soyad" type="text" /></td>
			</tr>
			
			<tr>
				<td>G�ncel Do�um Tarihi:</td>
				<td><input name="dogumTarihi" type="text"/></td>
			</tr>
	
		<tr>
			<td>Sehir:</td>
					<td><select name="sehirler.plakakod">
						<c:forEach items="${butunSehirler}" var="item">
							<option value="<c:out value="${item.plakakod}" />"><c:out
									value="${item.sehir}" /></option>
						</c:forEach>
					</select></td>	
		</tr>
	
			<tr>
				<td></td>
				<td><input type="submit" value="G�ncelle" /></td>
			</tr>		
		</table>
   </form:form>  
   </fieldset>
   
   
&nbsp; &nbsp;

<form method="post" name="plakakod" style="float: rigth;"action="filtreli">
		<select name="sehirler.plakakod">
		 <option value=0>T�m�</option>
			<c:forEach items="${butunSehirler}" var="i">
				<option value="<c:out value="${i.plakakod}" />"><c:out
						value="${i.sehir}" /></option>						
			</c:forEach>
		</select> 
		<input style="float: left;" type="submit" value="Filtrele">
		<a href="index">Geri Don</a> 
</form>
<br/>

<a href="index2"><input type="submit" value="Sehir Tablosu"></a> 

<br/>
<br/>

	<a href="/project/personelBilgi"><input type="submit" value="Rapor"></a>
	<br/><br/>
		<td>
	<form method="get" action="/project/filtreliRapor">
		<span>
		<<select name="sehirler.plakakod">
		 <option value=0> T�m� </option>
			<c:forEach items="${butunSehirler}" var="i">
				<option value="<c:out value="${i.plakakod}" />"><c:out
						value="${i.sehir}" /></option>						
			</c:forEach>
		</select>  <input type="submit" value="Filtreli Raporu �ndir"> 
	</form></td>	


</body>
</html>